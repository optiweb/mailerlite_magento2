#Mailerlite integration for Magento 2
This extension will connect your Magento 2 store with Mailerlite mailing service.

___

##Features:
- add subscribers via:
	- any newsletter form
	- user dashboard
	- admin panel
	- checkout
- unsubscribe
- choose different groups per store view
- enable debugging

##Compatibility:
Tested in Magento 2.1.8 and 2.2.4. It should work with all Magento 2 installations

##Known issues:
- once a user re-subscribes to your newsletter, they will recieve an error message 'Subscriber type is unsubscribed'. In Mailerlite they will not resubscribe, but in Magento they will

##Changelog
- `1.2.0` - Fix issue with different list ID's per store view
- `1.2.2` - Remove PHP 5 support, add Magento 2.3 support

##Installation:
1. `composer require optiweb/mailerlite_m2` OR
1. download a zip and upload contents to `app/code` folder
2. run `php bin/magento setup:upgrade` from the root directory of your Magento installation
3. go to Stores > Configuration > Optiweb > Mailerlite and enable it
4. login to Mailerlite and copy the API key to configuration from here <https://app.mailerlite.com/integrations/api/>
5. copy group ID to configuration. You can get it by opening it and copying it from the URL ie. https://app.mailerlite.com/subscribers/view/__XXXXXXX__

##Licence:
MIT. (see LICENCE.txt)
