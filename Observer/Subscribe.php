<?php
/**
 * @author Aleš Cankar
 * @copyright Copyright (c) 2018 Optiweb (https://www.optiweb.com)
 */

namespace Optiweb\MailerLite\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;

class Subscribe implements ObserverInterface
{
    protected $_config;
    protected $_messages;
    protected $_logger;
    
    const STATUS_SUBSCRIBED = 1;
    const STATUS_NOT_ACTIVE = 2;
    const STATUS_UNSUBSCRIBED = 3;
    const STATUS_UNCONFIRMED = 4;

    /**
     * Subscribe constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_config = $scopeConfig->getValue('optiweb_mailerlite/edit_ml_settings', ScopeInterface::SCOPE_STORE);
        $this->_messages = $messageManager;
        $this->_logger = $logger;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return bool
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $data = $observer->getEvent()->getDataObject()->getData();
        $email = $data['subscriber_email'];
        $change = $observer->getEvent()->getDataObject()->isStatusChanged();
        if ($this->_config['enable'] == "1" && $change) {
            if ($data['subscriber_status'] == self::STATUS_SUBSCRIBED) {
                $this->_subscribe($email);
            } else {
                if (isset($data['subscriber_status']) && $data['subscriber_status'] == self::STATUS_UNSUBSCRIBED) {
                    $this->_unsubscribe($email);
                }
            }
        }
        return true;
    }
    
    /**
     * Subscribe a user to a MailerLite list
     * @param $email
     * @return bool
     */
    private function _subscribe($email)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.mailerlite.com/api/v2/groups/{$this->_config['list_id']}/subscribers",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode(['email' => $email, 'type' => 'subscribed']),
            CURLOPT_HTTPHEADER => [
                "content-type: application/json",
                "x-mailerlite-apikey: {$this->_config['api_key']}",
            ],
        ]);
        $return = curl_exec($curl);
        $response = json_decode($return, true);
        $err = curl_error($curl);
        curl_close($curl);
        if ($this->_config['debug']) {
            if ($err || isset($response['error'])) {
                $this->_logger->debug('Optiweb_Mailerlite cUrl error:');
                if ($err) {
                    $this->_logger->debug($err);
                } else {
                    $this->_logger->debug($response['error']['message']);
                }
                $this->_messages->addErrorMessage(__($response['error']['message']));
                return false;
            } else {
                $this->_logger->debug('Optiweb_Mailerlite: Subscribed ' . $email);
            }
        }
        return true;
    }
    
    /**
     * Unsubscribe a user to a MailerLite list
     * @param $email
     * @return bool
     */
    private function _unsubscribe($email)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.mailerlite.com/api/v2/groups/{$this->_config['list_id']}/subscribers",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode(['email' => $email, 'type' => 'unsubscribed']),
            CURLOPT_HTTPHEADER => [
                "content-type: application/json",
                "x-mailerlite-apikey: {$this->_config['api_key']}",
            ],
        ]);
        $return = curl_exec($curl);
        $response = json_decode($return, true);
        $err = curl_error($curl);
        curl_close($curl);
        if ($this->_config['debug']) {
            if ($err || isset($response['error'])) {
                $this->_logger->debug('Optiweb_Mailerlite cUrl error:');
                if ($err) {
                    $this->_logger->debug($err);
                } else {
                    $this->_logger->debug($response['error']['message']);
                }
                $this->_messages->addErrorMessage(__($response['error']['message']));
                return false;
            } else {
                $this->_logger->debug('Optiweb_Mailerlite: Unubscribed ' . $email);
            }
        }
        return true;
    }
    
}